import 'package:flutter/material.dart';

const kFontPrimaryColor = Color(0xFF26283F);
const kFontSecondaryColor = Color(0xFF6E6F7C);

const kPrimaryColor = Color(0xFF414BA4);
const kPrimaryLightColor = Color(0xFFE4E5F7);
const kPrimarySuperLightColor = Color(0xFFF2F2FE);

const kRedColor = Color(0xFFCF6A5F);
const kRedLightColor = Color(0xFFFBE7E1);
const kRedSuperLightColor = Color(0xFFFDF3F2);

const kGreenColor = Color(0xFF51B067);
const kGreenLightColor = Color(0xFFDCF7E2);
const kGreenSuperLightColor = Color(0xFFF0FDF3);

const kBlueColor = Color(0xFF3E6FB6);
const kBlueLightColor = Color(0xFFD9E4FF);
const kBlueSuperLightColor = Color(0xFFE8EFFF);

const kFontPoppins = 'Poppins';

const TextStyle kTextHeading = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.bold,
);

const TextStyle kTitleStyle = TextStyle(
  fontSize: 14,
  height: 1.36,
  color: kFontPrimaryColor,
  fontWeight: FontWeight.bold,
);

const TextStyle kSubtitleStyle = TextStyle(
  fontSize: 12,
  height: 1.36,
  color: kFontSecondaryColor
);

const TextStyle kTextPrimaryButton = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w500,
);
