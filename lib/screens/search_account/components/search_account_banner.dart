import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:password_management/shared/styles.dart';

class SearchAccountBanner extends StatelessWidget {
  const SearchAccountBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        SizedBox(height: 116),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            height: 112,
            margin: const EdgeInsets.only(bottom: 4),
            color: kPrimaryColor,
          ),
        ),
        _BannerContent(),
        Container(
          height: 24,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
          ),
        ),
      ],
    );
  }
}

class _BannerContent extends StatelessWidget {
  const _BannerContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 16,
      left: 0,
      right: 0,
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Search',
              style: TextStyle(
                color: kFontSecondaryColor,
                fontSize: 14,
              ),
            ),
            Icon(
              Icons.search,
              size: 24,
              color: kFontSecondaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
