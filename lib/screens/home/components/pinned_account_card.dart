import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:password_management/shared/models/account.dart';
import 'package:password_management/shared/styles.dart';
import 'package:password_management/utils/account_color.dart';

class PinnedAccountCard extends StatelessWidget {
  const PinnedAccountCard(this.data, {Key? key}) : super(key: key);

  final Account data;

  @override
  Widget build(BuildContext context) {
    List<Color> colors = getColors(data.colorAccent);

    return Container(
      width: 200,
      decoration: BoxDecoration(
        color: colors[2],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 12),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 4,
              vertical: 2,
            ),
            margin: const EdgeInsets.only(
              left: 12,
              right: 24,
            ),
            decoration: BoxDecoration(
              color: colors[1],
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Text(
              data.category,
              style: TextStyle(
                fontSize: 10,
                color: colors[0],
              ),
            ),
          ),
          SizedBox(height: 12),
          Padding(
            padding: const EdgeInsets.only(
              left: 12,
              right: 24,
            ),
            child: Text(
              data.platformName,
              style: kTitleStyle,
              maxLines: 1,
            ),
          ),
          SizedBox(height: 4),
          Padding(
            padding: const EdgeInsets.only(
              left: 12,
              right: 24,
            ),
            child: Text(
              data.username,
              style: kSubtitleStyle,
              maxLines: 1,
            ),
          ),
          SizedBox(height: 22),
          Row(
            children: [
              Spacer(),
              _ButtonCopy(
                password: data.password,
                color: colors[0],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class _ButtonCopy extends StatelessWidget {
  const _ButtonCopy({
    Key? key,
    required this.password,
    required this.color,
  }) : super(key: key);

  final String password;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Clipboard.setData(
          ClipboardData(text: password),
        ).then(
          (_) => Fluttertoast.showToast(
            msg: "Password telah di salin",
            gravity: ToastGravity.BOTTOM,
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 12,
          vertical: 4,
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
        ),
        child: Row(
          children: [
            Text(
              'Salin Password',
              style: kTextPrimaryButton.copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
