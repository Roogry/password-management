import 'package:flutter/material.dart';
import 'package:password_management/shared/styles.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
    this.isFake = false,
    required this.onSearch,
  }) : super(key: key);

  final bool isFake;
  final ValueChanged<String> onSearch;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: TextField(
        enabled: !isFake,
        autofocus: !isFake,
        onChanged: onSearch,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide.none,
          ),
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 15,
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.only(left: 16, right: 13),
            child: Icon(
              Icons.search,
              size: 24,
              color: kFontSecondaryColor.withOpacity(.9),
            ),
          ),
          hintText: "Search here",
          hintStyle: kSubtitleStyle.copyWith(
            color: kFontSecondaryColor.withOpacity(.5),
          ),
        ),
      ),
    );
  }
}
