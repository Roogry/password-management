class Account {
  String accountId;
  String platformName;
  String username;
  String password;
  String category;
  String colorAccent;
  String imageIcon;
  bool isPinned;
  String createdAt;
  String updatedAt;
  String passwordUpdatedAt;

  Account({
    required this.accountId,
    required this.platformName,
    required this.username,
    required this.password,
    required this.category,
    required this.imageIcon,
    required this.colorAccent,
    required this.isPinned,
    required this.createdAt,
    required this.updatedAt,
    required this.passwordUpdatedAt,
  });
}

var accountList = [
  Account(
    accountId: 'AC0001',
    platformName: 'Google',
    username: 'jodiemantra@gmail.com',
    password: 'balala',
    category: 'lifestyle',
    colorAccent: 'primary',
    imageIcon: 'icons/ic_google.svg',
    isPinned: true,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  ),
  Account(
    accountId: 'AC0002',
    platformName: 'Instagram',
    username: 'sanchiajodie',
    password: 'balala',
    category: 'sosmed',
    colorAccent: 'red',
    imageIcon: 'icons/ic_instagram.svg',
    isPinned: true,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  ),
  Account(
    accountId: 'AC0003',
    platformName: 'WhatsApp',
    username: '6282147090943',
    password: 'balala',
    category: 'sosmed',
    colorAccent: 'green',
    imageIcon: 'icons/ic_whatsapp.svg',
    isPinned: false,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  ),
  Account(
    accountId: 'AC0004',
    platformName: 'Line',
    username: 'jodiemantra',
    password: 'balala',
    category: 'sosmed',
    colorAccent: 'green',
    imageIcon: 'icons/ic_line.svg',
    isPinned: false,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  )
];

var accountPinnedList = [
  Account(
    accountId: 'AC0001',
    platformName: 'Google',
    username: 'jodiemantra@gmail.com',
    password: 'balala',
    category: 'lifestyle',
    colorAccent: 'primary',
    imageIcon: 'icons/ic_google.svg',
    isPinned: true,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  ),
  Account(
    accountId: 'AC0002',
    platformName: 'Instagram',
    username: 'sanchiajodie',
    password: 'balala',
    category: 'sosmed',
    colorAccent: 'red',
    imageIcon: 'icons/ic_instagram.svg',
    isPinned: true,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  ),
  Account(
    accountId: 'AC0004',
    platformName: 'Line',
    username: 'jodiemantra',
    password: 'balala',
    category: 'sosmed',
    colorAccent: 'green',
    imageIcon: 'icons/ic_line.svg',
    isPinned: false,
    createdAt: '2021-06-05 10:00:00',
    updatedAt: '2021-06-05 10:00:00',
    passwordUpdatedAt: '2021-06-05 10:00:00',
  )
];
