import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:password_management/shared/models/account.dart';
import 'package:password_management/shared/styles.dart';
import 'package:password_management/utils/account_color.dart';

class AccountItem extends StatelessWidget {
  const AccountItem(this.data, {Key? key}) : super(key: key);

  final Account data;

  @override
  Widget build(BuildContext context) {
    List<Color> colors = getColors(data.colorAccent);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          Container(
            height: 60,
            width: 60,
            padding: const EdgeInsets.all(18),
            decoration: BoxDecoration(
              color: colors[1],
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset(
              'assets/' + data.imageIcon,
              color: colors[0],
            ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.platformName,
                  style: kTitleStyle,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                SizedBox(height: 4),
                Text(
                  data.username,
                  style: kSubtitleStyle,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                )
              ],
            ),
          ),
          TextButton(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
              child: Text(
                'SALIN',
                style: TextStyle(
                  color: kPrimaryColor,
                  fontSize: 12,
                ),
              ),
            ),
            onPressed: () {
              Clipboard.setData(
                ClipboardData(text: data.password),
              ).then(
                (_) => Fluttertoast.showToast(
                  msg: "Password telah di salin",
                  gravity: ToastGravity.BOTTOM,
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
