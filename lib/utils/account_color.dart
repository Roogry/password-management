
import 'package:flutter/material.dart';
import 'package:password_management/shared/styles.dart';

List<Color> getColors(String colors) {
    switch (colors){
      case 'red':
        return [kRedColor, kRedLightColor, kRedSuperLightColor];
      case 'green':
        return [kGreenColor, kGreenLightColor, kGreenSuperLightColor];
      case 'blue':
        return [kBlueColor, kBlueLightColor, kBlueSuperLightColor];
      default:
        return [
          kPrimaryColor,
          kPrimaryLightColor,
          kPrimarySuperLightColor
        ];
    }
}