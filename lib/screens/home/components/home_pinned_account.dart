import 'package:flutter/material.dart';
import 'package:password_management/screens/home/components/pinned_account_card.dart';
import 'package:password_management/shared/models/account.dart';
import 'package:password_management/shared/styles.dart';

import 'horizontal_list_view.dart';

class HomePinnedAccount extends StatelessWidget {
  const HomePinnedAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 32),
          child: Text(
            'Pinned',
            style: kTitleStyle.copyWith(fontSize: 18),
          ),
        ),
        SizedBox(height: 16),
        HorizontalListView(
          itemCount: accountPinnedList.length,
          itemBuilder: (context, index) {
            final Account account = accountPinnedList[index];
            return Padding(
              padding: EdgeInsets.only(
                left: (index == 0) ? 32 : 0,
                right: 16,
              ),
              child: InkWell(
                child: PinnedAccountCard(account),
              ),
            );
          },
        ),
        SizedBox(height: 24),
      ],
    );
  }
}
