import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:password_management/screens/home/components/home_banner_create_account.dart';
import 'package:password_management/screens/home/components/home_recently_account.dart';
import 'package:password_management/screens/home/components/home_pinned_account.dart';

import 'components/home_banner.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HomeBanner(),
              HomePinnedAccount(),
              HomeRecentlyAccount(),
              HomeBannerCreateAccount()
            ],
          ),
        ),
      ),
    );
  }
}
