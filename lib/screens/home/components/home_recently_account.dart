import 'package:flutter/material.dart';
import 'package:password_management/shared/components/account_item.dart';
import 'package:password_management/shared/models/account.dart';
import 'package:password_management/shared/styles.dart';

class HomeRecentlyAccount extends StatelessWidget {
  const HomeRecentlyAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 20),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                Text(
                  'Recently Added',
                  style: kTitleStyle.copyWith(fontSize: 18),
                ),
                Text(
                  'Lihat Semua',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
          ),
          SizedBox(height: 12),
          ...accountList
              .map((account) => AccountItem(account))
              .toList(),
        ],
      ),
    );
  }
}
