import 'package:flutter/material.dart';

class HorizontalListView extends StatelessWidget {
  /// WARNING: If you want to create a horizontal listview with a lot of items,
  /// please use `ListView.builder` instead of using this widget
  const HorizontalListView({
    required this.itemCount,
    required this.itemBuilder,
  }) : assert(itemCount >= 0);

  final int itemCount;
  final IndexedWidgetBuilder itemBuilder;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: [
        for (int i = 0; i < itemCount; i++) itemBuilder(context, i)
      ]),
    );
  }
}
