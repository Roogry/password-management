import 'package:flutter/material.dart';
import 'package:password_management/shared/components/account_item.dart';
import 'package:password_management/shared/components/search_field.dart';
import 'package:password_management/shared/models/account.dart';
import 'package:password_management/shared/styles.dart';

class SearchAccountScreen extends StatefulWidget {
  const SearchAccountScreen({Key? key}) : super(key: key);

  @override
  _SearchAccountScreenState createState() => _SearchAccountScreenState();
}

class _SearchAccountScreenState extends State<SearchAccountScreen> {
  List<Account> _filteredAccount = accountList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: null,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 32, right: 20),
              child: SearchField(
                onSearch: (keyword) {
                  if (keyword.trim().isEmpty) {
                    _filteredAccount = accountList;
                  } else {
                    _filteredAccount = accountList
                        .where((element) => (element.username
                              .toLowerCase()
                              .contains(keyword.toLowerCase()) ||
                          element.category
                              .toLowerCase()
                              .contains(keyword.toLowerCase()) ||
                          element.platformName
                              .toLowerCase()
                              .contains(keyword.toLowerCase()) ||
                          element.password
                              .toLowerCase()
                              .contains(keyword.toLowerCase())))
                        .toList();
                  }

                  setState(() {});
                },
              ),
            ),
            SizedBox(height: 24),
            Expanded(
              child: Container(
                padding: const EdgeInsets.fromLTRB(32, 24, 32, 32),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Hasil Pencarian',
                      style: kTextHeading,
                    ),
                    SizedBox(height: 16),
                    Expanded(
                      child: ListView.builder(
                        itemCount: _filteredAccount.length,
                        itemBuilder: (context, index) {
                          return AccountItem(_filteredAccount[index]);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
