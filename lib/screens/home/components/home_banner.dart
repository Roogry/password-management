import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:password_management/screens/search_account/search_account_screen.dart';
import 'package:password_management/shared/components/search_field.dart';
import 'package:password_management/shared/styles.dart';

class HomeBanner extends StatelessWidget {
  const HomeBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        SizedBox(height: 234),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            height: 230,
            margin: const EdgeInsets.only(bottom: 4),
            color: kPrimaryColor,
          ),
        ),
        _BannerContent(),
        Container(
          height: 32,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
          ),
        ),
      ],
    );
  }
}

class _BannerContent extends StatelessWidget {
  const _BannerContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 36,
      left: 0,
      right: 0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          children: [
            Stack(
              children: [
                Container(height: 32),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Text(
                    'Private Pass',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 21.5,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    height: 32,
                    width: 32,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Center(
                      child: IconButton(
                        padding: const EdgeInsets.all(4),
                        iconSize: 24,
                        icon: const Icon(Icons.add),
                        color: kPrimaryColor,
                        onPressed: () {
                          Fluttertoast.showToast(
                            msg: "Comming Soon",
                            gravity: ToastGravity.BOTTOM,
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 40),
            InkWell(
              onTap: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchAccountScreen()))
              },
              child: SearchField(
                isFake: true,
                onSearch: (value) {},
              ),
            )
          ],
        ),
      ),
    );
  }
}
