import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:password_management/shared/styles.dart';

class HomeBannerCreateAccount extends StatelessWidget {
  const HomeBannerCreateAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 24, 24, 40),
      child: Stack(
        children: [
          Container(
            height: 107,
            margin: const EdgeInsets.only(right: 8),
            decoration: BoxDecoration(
              color: kPrimarySuperLightColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 24, 56, 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Save New Password',
                    style: kTitleStyle.copyWith(color: kPrimaryColor),
                  ),
                  Text(
                    'Store all your password just in your local device',
                    style: kSubtitleStyle.copyWith(
                      color: Color(0xFF6772C1),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            child: SvgPicture.asset(
              'assets/icons/ic_shield.svg',
              color: kPrimaryColor,
              semanticsLabel:
                  'Make your password as strong as Warrior\'s Shield',
            ),
          )
        ],
      ),
    );
  }
}
